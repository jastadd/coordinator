FROM openjdk:11

ENV DEBIAN_FRONTEND noninteractive
ENV HOME /home/user
RUN useradd --create-home --home-dir $HOME user \
    && chmod -R u+rwx $HOME \
    && chown -R user:user $HOME

USER user

ENV GRADLE_USER_HOME /home/user/.gradle
COPY --chown=user:user gradle /temp/gradle
COPY --chown=user:user gradlew /temp/

WORKDIR /temp
RUN ./gradlew --no-daemon --version

COPY --chown=user:user . /coordinator/

WORKDIR /coordinator

RUN ./gradlew --no-daemon installDist

# parameters and config file have to be passed using "command"
ENTRYPOINT ["/bin/bash", "./build/install/coordinator/bin/coordinator"]
