#!/usr/bin/bash
shopt -s globstar

if [ ]
then
	:
else

# remove previous results
rm *-result.txt

#  --force-lang=Java,jrag --force-lang=Java,jadd
DEF_FILE=my_definitions.txt
echo "Export language definitions"
cloc --write-lang-def="$DEF_FILE" 2>cloc-errors.log
for f in cloc-def-*.txt;
do
	cat $f >> "$DEF_FILE"
done
echo "Running cloc with new definitions"
#  --ignored=bad-files.txt
CLOC_CMD="cloc --exclude-lang=JSON --read-lang-def=my_definitions.txt --exclude-list-file=.clocignore --quiet"
$CLOC_CMD --report-file="01-input-result.txt" --categorized=cats.txt --ignored=ignored.txt ../src/main/ja* 2>>cloc-errors.log

# $CLOC_CMD --report-file=base-gen-result.txt ../ros3rag.base/src/gen 2>>cloc-errors.log
$CLOC_CMD --report-file="02-gen-result.txt" ../src/gen 2>>cloc-errors.log

# CFC_CMD='grep -o 'if'\|'for'\|'return''
# echo "CFC stats" > cfcs.txt
# echo "base: $($CFC_CMD ../trainbenchmark-tool-jastadd-base/src/main/jastadd/{Helpers.*,Enums.jadd} | wc -l)" >> cfcs.txt
# cat cfcs.txt

# AC_CMD='grep -w 'syn'\|'inh''
# # \|inh
# echo "AC stats" > acs.txt
# echo "base: $($AC_CMD ../trainbenchmark-tool-jastadd-base/src/main/jastadd/{Helpers.*,Enums.jadd} | wc -l)" >> acs.txt
# cat acs.txt

fi

echo "LOC stats"
echo "Language                     files          blank        comment           code"
( for t in *-result.txt ; do echo -e "==> $t <=="; grep -v -e '---' -e 'SUM' -e 'Language' -e 'github' $t; done)
