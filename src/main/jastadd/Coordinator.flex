package de.tudresden.inf.st.coordinator.scanner;

import de.tudresden.inf.st.coordinator.parser.CoordinatorParser.Terminals;

%%

// define the signature for the generated scanner
%public
%final
%class CoordinatorScanner
%extends beaver.Scanner

// the interface between the scanner and the parser is the nextToken() method
%type beaver.Symbol
%function nextToken
%yylexthrow beaver.Scanner.Exception

// store line and column information in the tokens
%line
%column

// this code will be inlined in the body of the generated scanner class
%{
  private beaver.Symbol sym(short id) {
    return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
  }
  private beaver.Symbol symText(short id) {
    return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext().substring(1, yytext().length() - 1));
  }
%}

WhiteSpace = [ ] | \t | \f | \n | \r | \r\n
Identifier = [:jletter:][:jletterdigit:]*
Text = \" ([^\"]*) \"
Integer = [:digit:]+ // | "+" [:digit:]+ | "-" [:digit:]+

Comment = "//" [^\n\r]+

%%

// discard whitespace information and comments
{WhiteSpace}  { }
{Comment}     { }

// ** token definitions **
"components"         { return sym(Terminals.COMPONENTS); }
"component"          { return sym(Terminals.COMPONENT); }
"docker compose"     { return sym(Terminals.DOCKER_COMPOSE); }
"script"             { return sym(Terminals.SCRIPT); }
"report"             { return sym(Terminals.REPORT); }
"mqtt"               { return sym(Terminals.MQTT); }
"status"             { return sym(Terminals.STATUS); }
"after"              { return sym(Terminals.AFTER); }
"sec"                { return sym(Terminals.SEC); }
"in"                 { return sym(Terminals.IN); }
"if"                 { return sym(Terminals.IF); }
"start"              { return sym(Terminals.START); }
"using"              { return sym(Terminals.USING); }
"reqs"               { return sym(Terminals.REQS); }

","                  { return sym(Terminals.COMMA); }
"<"                  { return sym(Terminals.LT); }
"{"                  { return sym(Terminals.LB_CURLY); }
"}"                  { return sym(Terminals.RB_CURLY); }

{Identifier}         { return sym(Terminals.NAME); }
{Text}               { return symText(Terminals.TEXT); }
{Integer}            { return sym(Terminals.INTEGER); }
<<EOF>>              { return sym(Terminals.EOF); }
/* error fallback */
[^]                  { throw new Error("Illegal character '"+ yytext() +"' at line " + (yyline+1) + " column " + (yycolumn+1) + " in state " + yystate()); }
