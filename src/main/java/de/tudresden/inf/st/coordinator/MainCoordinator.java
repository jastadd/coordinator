package de.tudresden.inf.st.coordinator;

import beaver.Parser;
import de.tudresden.inf.st.coordinator.ast.*;
import de.tudresden.inf.st.coordinator.parser.CoordinatorParser;
import de.tudresden.inf.st.coordinator.scanner.CoordinatorScanner;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Entrypoint for Coordinator.
 *
 * @author rschoene - Initial contribution
 */
@Command(name = "Coordinator", version = "0.0.1", mixinStandardHelpOptions = true)
public class MainCoordinator implements Callable<Integer> {

  private static final String TOPIC_EXIT = "coordinator/exit";
  private static final String TOPIC_MODEL = "coordinator/model";
  private static final String TOPIC_STATUS = "coordinator/status";

  private Coordinator coordinator;
  private MqttHandler mainHandler;
  private final ScheduledThreadPoolExecutor executor = new ScheduledThreadPoolExecutor(2);

  @Option(names = { "--dry-run" }, description = "Do not run any external commands")
  boolean dryRun = false;

  @Option(names = { "--mqtt-host"}, description = "MQTT host to control this coordinator")
  String mqttHost = "localhost";

  @Option(names = { "-r", "--resolvingFailedStrategy"}, description = "Action if resolving of component fails, do nothing (IGNORE), print a message (WARNING), or throw a RuntimeException (EXIT)")
  ResolvingFailedStrategy resolvingFailedStrategy = ResolvingFailedStrategy.EXIT;

  @Parameters(description = "Coordinator config file to use")
  File configFile;

  public static void main(String[] args) {
    int exitCode = new CommandLine(new MainCoordinator())
        .setCaseInsensitiveEnumValuesAllowed(true)
        .execute(args);
    System.exit(exitCode);
  }

  @Override
  public Integer call() throws Exception {
    Coordinator.DRY_RUN = dryRun;
    Coordinator.RESOLVING_FAILED_STRATEGY = resolvingFailedStrategy;
    parsedBuild();
    return start();
  }

  private void parsedBuild() throws IOException, Parser.Exception {
    Reader in = Files.newBufferedReader(configFile.toPath());

    CoordinatorScanner scanner = new CoordinatorScanner(in);
    CoordinatorParser parser = new CoordinatorParser();

    coordinator = (Coordinator) parser.parse(scanner);

    in.close();
  }

  private int start() throws Exception {
    coordinator.ragconnectSetupMqttWaitUntilReady(2, TimeUnit.SECONDS);

    mainHandler = new MqttHandler().dontSendWelcomeMessage();
    mainHandler.setHost(mqttHost);
    if (!mainHandler.waitUntilReady(2, TimeUnit.SECONDS)) {
      System.err.println("Could not connect to MQTT host at '" + mqttHost + "'. Exiting.");
      return 1;
    }
    CountDownLatch exitCondition = new CountDownLatch(1);
    mainHandler.newConnection(TOPIC_EXIT, bytes -> exitCondition.countDown());
    mainHandler.newConnection(TOPIC_MODEL, bytes -> this.printStatus(new String(bytes)));
    Runtime.getRuntime().addShutdownHook(new Thread(this::close));

    for (Component comp : coordinator.getComponentList()) {
      if (comp.hasReportStrategy() && comp.getReportStrategy().isMqttReportStrategy()) {
        MqttReportStrategy mqttStrategy = comp.getReportStrategy().asMqttReportStrategy();
        if (mqttStrategy.getTopicPrefix() != null && !mqttStrategy.getTopicPrefix().isBlank()) {
          comp.connectStatus("mqtt://" + mqttHost + "/" + mqttStrategy.getTopicPrefix() + "/status");
        }
      }
      final String commandTopic;
      if (comp.getStartAsUp()) {
        // craft an internal mqtt-topic, and wire it up internally
        commandTopic = "coordinator/" + comp.getName();
        mainHandler.newConnection(commandTopic, bytes -> {
          try {
            boolean successfullyStarted = comp.getStartStrategy().start();
            comp.setStatus(successfullyStarted ? "ready" : "failedStart");
          } catch (IOException | InterruptedException e) {
            comp.setStatus("failedStart");
            e.printStackTrace();
          }
        });
      } else if (comp.hasReportStrategy() && comp.getReportStrategy().isMqttReportStrategy()) {
        commandTopic = comp.getReportStrategy().asMqttReportStrategy().getTopicPrefix() + "/command";
      } else {
        commandTopic = null;
      }
      if (commandTopic != null) {
        comp.connectNextCommand("mqtt://" + mqttHost + "/" + commandTopic, false);
      }
    }

    Set<Component> alreadyRunning = coordinator.getRunningComponents();
    for (Component comp : coordinator.getComponentList()) {
      if (alreadyRunning.contains(comp)) {
        comp.setStatus("up");
        if (comp.hasAutoSetStatus() && !comp.getStartAsUp()) {
          scheduleAutoSetStatus(comp);
        }
      }
      if (!alreadyRunning.contains(comp) && !comp.getStartAsUp()) {
        if (!comp.getStartStrategy().start()) {
          // component has failed to start
          comp.setStatus("failedStart");
        }
        if (comp.hasAutoSetStatus()) {
          scheduleAutoSetStatus(comp);
        }
      }
    }

    exitCondition.await();
    return 0;
  }

  private void scheduleAutoSetStatus(Component comp) {
    if (comp.getAutoSetStatus().isDelayAutoSetStatus()) {
      executor.schedule(() -> {
        System.out.println("Setting status of " + comp.getName() + " to " + comp.getAutoSetStatus().getStatus());
        comp.setStatus(comp.getAutoSetStatus().getStatus());
      }, comp.getAutoSetStatus().asDelayAutoSetStatus().getDelayInSeconds(), TimeUnit.SECONDS);
    } else if (comp.getAutoSetStatus().isMqttAutoSetStatus()) {
      mainHandler.newConnection(comp.getAutoSetStatus().asMqttAutoSetStatus().getTopic(), bytes -> {
        comp.setStatus(comp.getAutoSetStatus().getStatus());
      });
    }
  }

  private void printStatus(String message) {
    String content = message.contains("detail") ?
        ("Model-Status (detailed):\n" + coordinator.details()) : ("Model-Status:\n" + coordinator.prettyPrint());
    System.out.println(content);
    if (mainHandler != null) {
      mainHandler.publish(TOPIC_STATUS, content.getBytes(StandardCharsets.UTF_8));
    }
  }

  private void close() {
    if (coordinator != null) {
      coordinator.ragconnectCloseConnections();
    }
    if (mainHandler != null) {
      mainHandler.close();
    }
  }

}
